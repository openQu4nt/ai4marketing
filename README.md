AI 4 Marketing
==============

This are notes about Artificial Intelligence applications, infrastructure, tools and software architecture.

We have an "Awesome AI for Marketing", a curated list of resources, tools and links.

The content is edited on Jupyter Notebooks and markdown docs, feel free to contribute.

$e^{i\tau}=1$
